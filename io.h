#ifndef IO_H
#define IO_H

#include <stdint.h>

/**Method to print specified error messageto screen & exit the program.*/
void fatal_error(const char *message);

/**Method to write a single byte with value val to specified file, out.*/
void write_byte(FILE *out, char val);

/**Method to write n bytes, with values contained in data, to file, out.*/
void write_bytes(FILE *out, const char data[], unsigned n);

/**Method to write a uint16 value with value val to specified file out.*/
void write_u16(FILE *out, uint16_t value);

/**Method to write a uint32 value with value val to specified file out.*/
void write_u32(FILE *out, uint32_t value);

/**Method to write a sint16 value with value val to specified file out.*/
void write_s16(FILE *out, int16_t value);

/**Method to write n sint16 values, with values contained in buf, to file out.*/
void write_s16_buf(FILE *out, const int16_t buf[], unsigned n);

/**Method to read a single byte with from specified file in & store in val.*/
void read_byte(FILE *in, char *val);

/**Method to read n bytes from file in & store in data.*/
void read_bytes(FILE *in, char data[], unsigned n);

/**Method to read a uint16 value from specified file in & store in val.*/
void read_u16(FILE *in, uint16_t *val);

/**Method to read a uint32 value from specified file in & store in val.*/
void read_u32(FILE *in, uint32_t *val);

/**Method to read a sint16 value from specified file in & store in val.*/
void read_s16(FILE *in, int16_t *val);

/**Method to read n sint16 values from specified file in & store in buf.*/
void read_s16_buf(FILE *in, int16_t buf[], unsigned n);

#endif /* IO_H */
