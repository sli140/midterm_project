commit 0999d289c1ca68fc32d2c509ead832cde1a81ecd
Merge: df2fc19 f450988
Author: Muna <Muna@Munas-MacBook-Air.local>
Date:   Thu Oct 17 18:01:15 2019 -0400

    Merge branch 'master' of https://bitbucket.org/sli140/midterm_project

commit df2fc19c1bfb4963b57b8e5f5e89c3f2a0f7d65a
Author: Muna <Muna@Munas-MacBook-Air.local>
Date:   Thu Oct 17 18:01:07 2019 -0400

    render_echo fixed

commit f450988c2e58c6bda7fb06525b5eee6965c7a6fc
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 21:59:59 2019 +0000

    render_tone.c pointers fixed

commit 5a619747e7ef8b18b91f624cf547408742c7d48a
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 21:58:44 2019 +0000

    render_echo.c pointers fixed

commit b51cf92a8232ad62974a0cf3ef8d83d1697b77d6
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 21:42:44 2019 +0000

    difference found in render_echo.c

commit 53823604fb86a7238d952779e7c794bf63cb63ea
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 21:32:58 2019 +0000

    render_echo.c fixed fully

commit 95742bf081da6c783fcfef6573b22be467cdd685
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 21:30:30 2019 +0000

    render_echo.c fixed

commit 65e5722465a2b3e65649b1d47410e32d033b16ec
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 21:23:47 2019 +0000

    render_song.c  maybe fixed

commit 0b6cee337b39227c06433191c56d2f9d370ab0e1
Author: Muna <Muna@Munas-MacBook-Air.local>
Date:   Thu Oct 17 17:15:34 2019 -0400

    render song still not working

commit 42e8f372f93e3e4dfc01c9d4741d656c34d865c0
Author: Muna <Muna@Munas-MacBook-Air.local>
Date:   Thu Oct 17 16:56:12 2019 -0400

    render_song not working

commit 1d5e6b2159e0151d459c89b284a09369c85cff1e
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:51:35 2019 +0000

    render_song.c edited online with Bitbucket

commit c21b89f3e419e8f0183cd22ff8aed946cb1d7a7f
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:50:32 2019 +0000

    render_song.c edited online with Bitbucket

commit 3f0e4de69796f1c9d286dd7a925a65e501f7679e
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:49:34 2019 +0000

    render_song.c edited online with Bitbucket

commit f19e320342585331c0f027ed4ca5588b6759a5eb
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:48:52 2019 +0000

    render_song.c edited online with Bitbucket

commit 0119125a771cf492b593e3e0f94b338e510bcb6a
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:25:53 2019 +0000

    wave.c warning fixed

commit f5fb8416c906dc42c380f12f77f0ef0717ec517a
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:23:52 2019 +0000

    wave.h comments added

commit c44cea849aa6d672de553f4456e89c56e82d9363
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:21:20 2019 +0000

    io.c names added

commit 082f130b6112d52f21f5e4eb30674a3c03c00fe8
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:20:03 2019 +0000

    io.h comments added

commit 136f9d1f8a2d09581302c97cf521b702399c53b2
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:16:00 2019 +0000

    render_tone.c edited online with Bitbucket

commit bcdf0e75ff9815ef9aae2ff3c68c1bfceaf7ce0a
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:15:16 2019 +0000

    added comments & fixed formatting for render_echo.c

commit e210f61f86c0672451d0b38337de01849bf742ab
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:08:49 2019 +0000

    render_song.c edited online with Bitbucket

commit 4464b349c6f8766541feb8c10d5d1e816b0880a9
Merge: c0b70ab a799741
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:07:44 2019 +0000

    Merged in Munachiso/added-comments-to-fixed-formatting-of-r-1571342842198 (pull request #1)
    
    added comments to & fixed formatting of render_tone.c

commit a799741f5be0d186b3b152b2ead9a5c2b5cf3ed8
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:07:22 2019 +0000

    added comments to & fixed formatting of render_tone.c

commit c0b70ab3ed59203927878098026422529cbc1f04
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 20:04:56 2019 +0000

    fixed wave.c

commit e592c4d944ade61a9e86871cd1e2400d42a4460c
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 19:57:30 2019 +0000

    added comments to & fixed formatting for render_song.c

commit c7e1c52318ced63b9fe4ee5fcde6a529b96f2c94
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 19:42:19 2019 +0000

    added comments & fixed formatting in wave.c

commit 1bc97672afa6e6c132be7c481cb30b28f343cbb6
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 19:25:44 2019 +0000

    comments added to io.c & formatting fixed

commit addd6e7512abaeec7dad587909a7dd437c5e49f2
Author: Munachiso <migboko1@jhu.edu>
Date:   Thu Oct 17 19:10:00 2019 +0000

    io.c edited online with Bitbucket

commit 3c8ed8538748099360baacef0b5aa7df4a462f01
Merge: 750e75d 6ed7477
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Thu Oct 17 09:17:59 2019 -0400

    Merge branch 'master' of https://bitbucket.org/sli140/midterm_project
    
    pull to test

commit 6ed7477a1414981b5439ae1c29256eac18d88ad6
Author: Munachiso Igboko <migboko1@jhu.edu>
Date:   Wed Oct 16 23:56:15 2019 -0400

    saw wave difference too high. everything works

commit 750e75d271864e7f9733d7d215281e81b6fd72e6
Author: Shiyu Li <sli140@ugrad8.cs.jhu.edu>
Date:   Wed Oct 16 22:39:53 2019 -0400

    test

commit 1eaba2470c4cd1fa6463859111e9ac6269ca57eb
Author: Munachiso Igboko <migboko1@jhu.edu>
Date:   Wed Oct 16 22:23:53 2019 -0400

    render_song & render_echo working

commit dc2df30e89381a2ce17e9418b579060082ca50ea
Author: Munachiso Igboko <migboko1@jhu.edu>
Date:   Wed Oct 16 20:53:29 2019 -0400

    render_echo working

commit 349236c4a46b7ecdc939cdffbd5441aa574ca268
Author: Munachiso Igboko <migboko1@jhu.edu>
Date:   Wed Oct 16 20:03:36 2019 -0400

    fixed makefile

commit fd0165b72cc5bb8092b9b918c60ad22d3e5f5098
Author: Munachiso Igboko <migboko1@jhu.edu>
Date:   Wed Oct 16 20:00:26 2019 -0400

    added song and wave

commit d30b3ca2735a6d65a2ea00e89a7a9cd5b892cf2e
Author: Munachiso Igboko <migboko1@jhu.edu>
Date:   Wed Oct 16 19:42:03 2019 -0400

    added render_echo.c

commit 7a4387b215bfe425d23754516c11134fff311d14
Author: Munachiso Igboko <migboko1@jhu.edu>
Date:   Wed Oct 16 19:40:18 2019 -0400

    test

commit d382fb59d17b4a0e763456e1356f1c12708e7d9b
Author: Munachiso <migboko1@jhu.edu>
Date:   Wed Oct 16 23:09:02 2019 +0000

    .render_tone.c.swp edited online with Bitbucket

commit 67f39bebe77901215f13db2c8d51b9c29cb48104
Author: Munachiso <migboko1@jhu.edu>
Date:   Wed Oct 16 23:08:53 2019 +0000

    .render_song.c.swp edited online with Bitbucket

commit cbf848e1922d6ed67cbd8b0c84701dfba4a6edc8
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 16:07:58 2019 -0400

    render_song gets first note correct but the rest is all a pause

commit 127896546968e18ecb2bcf71079bf2b92049c3d6
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 15:11:25 2019 -0400

    need to fix saw & render_song

commit 5f996d567dd05844f6894e5e676a5eedcb745dd6
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 14:47:04 2019 -0400

    render_song reads half input then segmentation fault

commit 68d567b981846edde20e428382e680caaf113f59
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 13:59:46 2019 -0400

    sine wave & square wave both correct. Sawtooth wave not producing any sound

commit afbb0bc0e9165c09a4d3fe261756212bff000286
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 13:57:02 2019 -0400

    sine_wave function fixed, producing correct output

commit a21828e5565dc5f2f15e48dd9fcb077a1c104324
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 13:22:02 2019 -0400

    render_song.c

commit adda7210bd1aa70a33d89647df0279251b85f4e7
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 11:58:10 2019 -0400

    render_song.c fscanf not working properly

commit 0bae19f5b103c78522d0083802daea7ac7ea966f
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 03:03:33 2019 -0400

    second try song

commit 44bc13111bbac0bdc3ac72e6059e30bfc841d058
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 02:49:03 2019 -0400

    attempt to deit render_song.c

commit c1962741b5000f6ef4b9aa9a986571d32fd22f06
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Wed Oct 16 00:10:03 2019 -0400

    render_song creating song of correct size, wrong sound

commit 530dc68c63ad5f8d2e4a322213d9e80723f22a7d
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 23:55:06 2019 -0400

    render song reading input & calculating frequency correctly

commit 6252e15ee18bbb0b1ee0651489e97d0926626913
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 23:32:14 2019 -0400

    edited render_song, compiles  with 1 warning

commit 60ca29ee385410b29ef059538cdb0fa2f310946c
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 23:13:30 2019 -0400

    update makefile for render_song didn't work

commit 7fb8614dd5088e26f6cd0ba99880e0b7a9a73f1a
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 23:02:10 2019 -0400

    created render_song.c

commit 12318b7a154e13ef273afdda4a93b500239ddbb4
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 22:29:04 2019 -0400

    fix formating not working

commit ae9d3f4407fafb05a5b662184daf6506f38c820f
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 21:53:05 2019 -0400

    edited wave.c

commit e5879af8bfc02ea8ee71f688064fe6900fdff6d9
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 21:43:07 2019 -0400

    io files edit

commit aa635e867a86a80b5a333d363842118a2ccc87e2
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 21:28:50 2019 -0400

    edited

commit 1a3ee372eb19bb5758d88f4951ece9b11a470257
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 21:28:02 2019 -0400

    sine function modified

commit f14e0412287e2a6a83a246747f50204d62c4f123
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 21:14:26 2019 -0400

    modified sine again again

commit 85c3bc3466f13041c44e1e677257ac6eda633ae6
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 21:11:20 2019 -0400

    sine function modified again

commit 9c835c493c27be028f12f35c1a24ed72cae6edc5
Merge: 60890fd 2538a55
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 21:07:56 2019 -0400

    sine function fixed

commit 60890fddf02b6509e509929ffda1b679f3726398
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 20:38:07 2019 -0400

    render_tone edited

commit 2538a5530da1b8cc170d3d162f3ec482c337a192
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 20:33:23 2019 -0400

    Makefile fixed

commit df616cb385d274bb231688376a32a58778d564c8
Merge: 14e1a5a 183da4f
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 20:05:48 2019 -0400

    render_tone not working

commit 14e1a5a16c46bb5473561a143ddaabf44697635b
Author: Munachiso Lgboko <migboko1@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 20:04:56 2019 -0400

    render_tone written

commit 183da4fbf0eb67e40d1a31822bc721ccb1cf468b
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 19:50:18 2019 -0400

    edited Makefile

commit 2887b20958fd5675250d37765ae9c6969b000887
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 19:29:08 2019 -0400

    update tone for makefile

commit ae5eca9a185bceee05d15cdbf82da16f12a31a31
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 15:11:31 2019 -0400

    finished wave.c

commit e28240c16805c654142d7fb00e3bdabc92ef22cb
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Tue Oct 15 01:35:42 2019 -0400

    edited render wave funcs

commit 51e343c22d6d9da149be6ce8ee8aedd0cc9dd0b5
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Mon Oct 14 23:43:11 2019 -0400

    io written draft

commit af407a3aa248d2a106a778a125fbe299e0765a47
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Mon Oct 14 21:45:14 2019 -0400

    modified io

commit b43296ed24ce1ea4575d75ec6434e22fb98e1cab
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Mon Oct 14 20:18:56 2019 -0400

    start2

commit 87bccac3040275d2e0395ebd6800f027089813df
Author: Shiyu Li <sli140@ugradx.cs.jhu.edu>
Date:   Mon Oct 14 20:10:35 2019 -0400

    start
