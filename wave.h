/**Shiyu Li (sli140) 
Munachiso Igboko (migboko1)
*/

#ifndef WAVE_H
#define WAVE_H

#include <stdint.h>

#define PI                 3.14159265358979323846
#define SAMPLES_PER_SECOND 44100u
#define NUM_CHANNELS       2u
#define BITS_PER_SAMPLE    16u

/* voices */
#define SINE       0
#define SQUARE     1
#define SAW        2
#define NUM_VOICES 3 /* one greater than maximum legal voice */

/*
 * Write a WAVE file header to given output stream.
 * Format is hard-coded as 44.1 KHz sample rate, 16 bit
 * signed samples, two channels.
 *
 * Parameters:
 *   out - the output stream
 *   num_samples - the number of (stereo) samples that will follow
 */
void write_wave_header(FILE *out, unsigned num_samples);

/*
 * Read a WAVE header from given input stream.
 * Calls fatal_error if data can't be read, if the data
 * doesn't follow the WAVE format, or if the audio
 * parameters of the input WAVE aren't 44.1 KHz, 16 bit
 * signed samples, and two channels.
 *
 * Parameters:
 *   in - the input stream
 *   num_samples - pointer to an unsigned variable where the
 *      number of (stereo) samples following the header
 *      should be stored
 */
void read_wave_header(FILE *in, unsigned *num_samples);

/**Method to render a sine wave with num_samples pressure values calculated from given freq_hz & amplitude & stored in the 
appropriate channel in buf.*/
void render_sine_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude);

/**Method to render sine waves for both channels.*/
void render_sine_wave_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude);

/**Method to render a square wave with num_samples pressure values calculated from given freq_hz & amplitude & stored in the 
appropriate channel in buf.*/
void render_square_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude);

/**Method to render square waves for both channels.*/
void render_square_wave_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude);

/**Method to render a saw wave with num_samples pressure values calculated from given freq_hz & amplitude & stored in the 
appropriate channel in buf.*/
void render_saw_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude);

/**Method to render saw waves for both channels.*/
void render_saw_wave_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude);

/**Method to render the waves specified by voice in the appropriate channel.*/
void render_voice(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude, unsigned voice);

/**Method to render the waves specified by voice in both channels.*/
void render_voice_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude, unsigned voice);
#endif /* WAVE_H */
