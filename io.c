/**Shiyu Li (sli140) 
Munachiso Igboko (migboko1)
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "io.h"

/**Method to print specified error messageto screen & exit the program.*/
void fatal_error(const char *message) {
    fprintf(stderr, "Error: %s\n", message);
    exit(1); 
}

/**Method to write a single byte with value val to specified file, out.*/
void write_byte(FILE *out, char val) {
    int write = fputc(val, out);
    if (write == EOF) {
        const char *message = "failed to write byte.\n";
        fatal_error(message);
    }
}

/**Method to write n bytes, with values contained in data, to file, out.*/
void write_bytes(FILE *out, const char data[], unsigned n) {
    if (ferror(out) != 0) {
        const char *message = "failed to write bytes.\n";
        fatal_error(message);
    }
    else {
        unsigned i = 0;
        for (; i < n; i++) {
            write_byte(out, data[i]);
        }
    }
}

/**Method to write a uint16 value with value val to specified file out.*/
void write_u16(FILE *out, uint16_t value) {
    if (ferror(out) != 0) {
        const char *message = "failed to write u16.\n";
        fatal_error(message);
    } 

    char least = value & 0xFF; //Obtain the least singificant byte
    char most = (value >> 8) & 0xFF; //Obtain the most singificant byte
    int l = fputc(least, out); //write the byte to out
    if (l == EOF) {
        fatal_error("failed to write_u16 least\n");
    }
    int m = fputc(most, out); //write the byte to out
    if (m == EOF) {
        fatal_error("failed to write_u16 most\n");
    }
}

/**Method to write a uint32 value with value val to specified file out.*/
void write_u32(FILE *out, uint32_t value) {
    if (ferror(out) != 0) {
        const char *message = "failed to write u32.\n";
        fatal_error(message);
    }
    
    char v1 = value & 0xFF; //Obtain the least singificant byte & write to out
        int c1 = fputc(v1, out);
        if (c1 == EOF) {
            fatal_error("write_u32 failed\n");
        }
    char v2 = (value >> 8) & 0xFF; //Obtain the next least singificant byte & write to out
        int c2 = fputc(v2, out);
        if (c2 == EOF) {
            fatal_error("write_u32 failed\n");
        }
    char v3 = (value >> 16) & 0xFF; //Obtain the second most singificant byte & write to out
        int c3 = fputc(v3, out);
        if (c3 == EOF) {
            fatal_error("write_u32 failed\n");
        }

    char v4 = (value >> 24) & 0xFF; //Obtain the most singificant byte & write to out
        int c4 = fputc(v4, out);
        if (c4 == EOF) {
            fatal_error("write_u32 failed\n");
        }    
    
}

/**Method to write a sint16 value with value val to specified file out.*/
void write_s16(FILE *out, int16_t value) {
    if (ferror(out) != 0) {
        const char *message = "failed to write s16.\n";
        fatal_error(message);
    } 

    char least = value & 0xFF; //Obtain the least singificant byte
    char most = (value >> 8) & 0xFF; //Obtain the most singificant byte
    int l = fputc(least, out); //write the byte to out
    if (l == EOF) {
        fatal_error("failed to write_s16 least\n");
    }
    int m = fputc(most, out); //write the byte to out
    if (m == EOF) {
        fatal_error("failed to write_s16 most\n");
    }

}

/**Method to write n sint16 values, with values contained in buf, to file out.*/
void write_s16_buf(FILE *out, const int16_t buf[], unsigned n) {
    if (ferror(out) != 0) {
        const char *message = "write_s16_buf failed.\n";
        fatal_error(message);
    }
    else {
        unsigned i = 0;
        for (; i < n; i++) {
            write_s16(out, buf[i]); //write each sint16 value in buf to out
        }
    }

}

/**Method to read a single byte with from specified file in & store in val.*/
void read_byte(FILE *in, char *val) {
    //check error of file
    if (feof(in) != 0) {
        fatal_error("failed to read byte\n");
    } else {
        int c = fgetc(in);
        if (c == EOF) {
            fatal_error("failed to read byte\n");
        }
        *val = c;
    }
}

/**Method to read n bytes from file in & store in data.*/
void read_bytes(FILE *in, char data[], unsigned n) {
    if (feof(in) != 0) {
        const char *message = "failed to read bytes.\n";
        fatal_error(message);
    }
    else {
        unsigned i = 0;
        for (; i < n; i++) {
            char *p = &data[i];
            read_byte(in, p);
        }
    }
}

/**Method to read a uint16 value from specified file in & store in val.*/
void read_u16(FILE *in, uint16_t *val) {
        if (feof(in) != 0) {
            const char *message = "failed to read u16.\n";
            fatal_error(message);
        } else {

        int v1 = fgetc(in);
        if (v1 == EOF) {
            fatal_error("failed to read u16.\n");
        }
        int v2 = fgetc(in);
        if (v2 == EOF) {
            fatal_error("failed to read u16.\n");
        }
        *val = v1 | (v2 << 8); //Construct bytes into little endian format
    }
}

/**Method to read a uint32 value from specified file in & store in val.*/
void read_u32(FILE *in, uint32_t *val) {
    if (feof(in) != 0) {
        const char *message = "failed to read u32.\n";
        fatal_error(message);
    } else {
   
        int v1 = fgetc(in);
        if (v1 == EOF) {
            fatal_error("failed to read u32.\n");
        }

        int v2 = fgetc(in);
        if (v2 == EOF) {
            fatal_error("failed to read u32.\n");
        }
       
        int v3 = fgetc(in);
        if (v3 == EOF) {
            fatal_error("failed to read u32.\n");
        }

        int v4 = fgetc(in);
        if (v4 == EOF) {
            fatal_error("failed to read u32.\n");
        }

        *val = v1 | (v2 << 8) | (v3 << 16) | (v4 << 24); //Construct bytes into little endian format
    }
    

}

/**Method to read a sint16 value from specified file in & store in val.*/
void read_s16(FILE *in, int16_t *val) {
   if (feof(in) != 0) {
            const char *message = "failed to read s16.\n";
            fatal_error(message);
        } else {

        int v1 = fgetc(in);
        if (v1 == EOF) {
            fatal_error("failed to read s16.\n");
        }
        int v2 = fgetc(in);
        if (v2 == EOF) {
            fatal_error("failed to read s16.\n");
        }
        *val = v1 | (v2 << 8); //Construct bytes into little endian format
    }
 
}

/**Method to read n sint16 values from specified file in & store in buf.*/
void read_s16_buf(FILE *in, int16_t buf[], unsigned n) {
    if (feof(in) != 0) {
        fatal_error("failed to read byte\n"); 
    } else {
        unsigned i = 0;
        for (; i < n; i++) {
            int16_t *p = &buf[i];
            read_s16(in, p);
        }
    }
}
 
