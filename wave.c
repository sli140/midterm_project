/**Shiyu Li (sli140) 
Munachiso Igboko (migboko1)
*/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "io.h"
#include "wave.h"

/*
 * Write a WAVE file header to given output stream.
 * Format is hard-coded as 44.1 KHz sample rate, 16 bit
 * signed samples, two channels.
 *
 * Parameters:
 *   out - the output stream
 *   num_samples - the number of (stereo) samples that will follow
 */
void write_wave_header(FILE *out, unsigned num_samples) {
  /*
   * See: http://soundfile.sapp.org/doc/WaveFormat/
Microsoft WAVE soundfile format
The canonical WAVE format starts with the RIFF header: 0 4 ChunkID Contains the letters "RIFF" in ASCII form (0x52494646 big-endian form). 4 4 ChunkSize 36 + SubChunk2Size, or more precisely: 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size) This is the size of the rest of the chunk following this number.
soundfile.sapp.org
   */

  uint32_t ChunkSize, Subchunk1Size, Subchunk2Size;
  uint16_t NumChannels = NUM_CHANNELS;
  uint32_t ByteRate = SAMPLES_PER_SECOND * NumChannels * (BITS_PER_SAMPLE/8u);
  uint16_t BlockAlign = NumChannels * (BITS_PER_SAMPLE/8u);

  /* Subchunk2Size is the total amount of sample data */
  Subchunk2Size = num_samples * NumChannels * (BITS_PER_SAMPLE/8u);
  Subchunk1Size = 16u;
  ChunkSize = 4u + (8u + Subchunk1Size) + (8u + Subchunk2Size);

  /* Write the RIFF chunk descriptor */
  write_bytes(out, "RIFF", 4u);
  write_u32(out, ChunkSize);
  write_bytes(out, "WAVE", 4u);

  /* Write the "fmt " sub-chunk */
  write_bytes(out, "fmt ", 4u);       /* Subchunk1ID */
  write_u32(out, Subchunk1Size);
  write_u16(out, 1u);                 /* PCM format */
  write_u16(out, NumChannels);
  write_u32(out, SAMPLES_PER_SECOND); /* SampleRate */
  write_u32(out, ByteRate);
  write_u16(out, BlockAlign);
  write_u16(out, BITS_PER_SAMPLE);

  /* Write the beginning of the "data" sub-chunk, but not the actual data */
  write_bytes(out, "data", 4);        /* Subchunk2ID */
  write_u32(out, Subchunk2Size);
}

/*
 * Read a WAVE header from given input stream.
 * Calls fatal_error if data can't be read, if the data
 * doesn't follow the WAVE format, or if the audio
 * parameters of the input WAVE aren't 44.1 KHz, 16 bit
 * signed samples, and two channels.
 *
 * Parameters:
 *   in - the input stream
 *   num_samples - pointer to an unsigned variable where the
 *      number of (stereo) samples following the header
 *      should be stored
 */
void read_wave_header(FILE *in, unsigned *num_samples) {
  char label_buf[4];
  uint32_t ChunkSize, Subchunk1Size, SampleRate, ByteRate, Subchunk2Size;
  uint16_t AudioFormat, NumChannels, BlockAlign, BitsPerSample;

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "RIFF", 4u) != 0) {
    fatal_error("Bad wave header (no RIFF label)");
  }

  read_u32(in, &ChunkSize); /* ignore */

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "WAVE", 4u) != 0) {
    fatal_error("Bad wave header (no WAVE label)");
  }

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "fmt ", 4u) != 0) {
    fatal_error("Bad wave header (no 'fmt ' subchunk ID)");
  }

  read_u32(in, &Subchunk1Size);
  if (Subchunk1Size != 16u) {
    fatal_error("Bad wave header (Subchunk1Size was not 16)");
  }

  read_u16(in, &AudioFormat);
  if (AudioFormat != 1u) {
    fatal_error("Bad wave header (AudioFormat is not PCM)");
  }

  read_u16(in, &NumChannels);
  if (NumChannels != NUM_CHANNELS) {
    fatal_error("Bad wave header (NumChannels is not 2)");
  }

  read_u32(in, &SampleRate);
  if (SampleRate != SAMPLES_PER_SECOND) {
    fatal_error("Bad wave header (Unexpected sample rate)");
  }

  read_u32(in, &ByteRate); /* ignore */

  read_u16(in, &BlockAlign); /* ignore */

  read_u16(in, &BitsPerSample);
  if (BitsPerSample != BITS_PER_SAMPLE) {
    fatal_error("Bad wave header (Unexpected bits per sample)");
  }

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "data", 4u) != 0) {
    fatal_error("Bad wave header (no 'data' subchunk ID)");
  }

  /* finally we're at the Subchunk2Size field, from which we can
   * determine the number of samples */
  read_u32(in, &Subchunk2Size);
  *num_samples = Subchunk2Size / NUM_CHANNELS / (BITS_PER_SAMPLE/8u);
}

/**Method to render a sine wave with num_samples pressure values calculated from given freq_hz & amplitude & stored in the 
appropriate channel in buf.*/
void render_sine_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude) {

    unsigned size = 2 * num_samples; //there are 2 channels
    unsigned t = 0; //to keep track of index

    for (unsigned i = 0 + channel; i < size; i += 2) { //For each index in the specified channel
        float time = ((float)t) / SAMPLES_PER_SECOND; //Calculate time
        float pressure = amplitude * (float)sin(time * freq_hz * 2 * PI); //Calculate pressure
        int32_t scaled_p = 0; 

        //Scale the pressure by the appropriate maximum int16 values
        if (pressure < 0) {
            scaled_p = pressure * 32768;
        }
        else if (pressure > 0) {
            scaled_p = pressure * 32767;
        }

        //Clamp the pressure at the appropriate maximum values if they are exceeded
        if (buf[i] + (int16_t)scaled_p > 32767) {
            buf[i] = 32767;
        } else if (buf[i] + (int16_t)scaled_p < -32768) {
            buf[i] = -32768;
        }else {
            buf[i] += (int16_t)scaled_p; //Continue adding the pressure, as it is additive
        }


        t++; //Increment index
    }
}

/**Method to render sine waves for both channels.*/
void render_sine_wave_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude) {
    //Call both render_sine_wave with both channels:  0 for left, 1 for right
    render_sine_wave(buf, num_samples, 0, freq_hz, amplitude);
    render_sine_wave(buf, num_samples, 1, freq_hz, amplitude);
    

}

/**Method to render a square wave with num_samples pressure values calculated from given freq_hz & amplitude & stored in the 
appropriate channel in buf.*/
void render_square_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude) {
    
    int16_t max = 32767 * amplitude; //max int16 value scaled by amplitude
    int16_t min = -32768 * amplitude; //min int16 value scaled by amplitude
    unsigned size = 2 * num_samples; 
    unsigned t = 0;

    for (unsigned i = 0 + channel; i < size; i += 2) {
        float time = (float)t / SAMPLES_PER_SECOND;
        float pressure = amplitude * (float)sin(time * freq_hz * 2 * PI);
        int32_t scaled_p = 0;
	    
        //Scale the pressure by the appropriate maximum int16 values
        if (pressure < 0) {
            scaled_p = pressure * 32768;
        }
        else if (pressure > 0) {
            scaled_p = pressure * 32767;
        }
	
	    //To all positive pressure values, add the scaled max
        if (scaled_p > 0) {
            buf[i] += max;
        }
        //To all negative pressure values, add the scaled min
        else if (scaled_p < 0) {
            buf[i] += min;
        }

	    //Clamp the pressure at the appropriate maximum values if they are exceeded
        if (buf[i] + (int16_t)scaled_p > 32767) {
            buf[i] = 32767;
        } else if (buf[i] + (int16_t)scaled_p < -32768) {
            buf[i] = -32768;
        }

	    t++; //Increment index
    }
}

/**Method to render square waves for both channels.*/
void render_square_wave_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude) {
    //Call both render_square_wave with both channels:  0 for left, 1 for right    
    render_square_wave(buf, num_samples, 0, freq_hz, amplitude);
    render_square_wave(buf, num_samples, 1, freq_hz, amplitude);
}

/**Method to render a saw wave with num_samples pressure values calculated from given freq_hz & amplitude & stored in the 
appropriate channel in buf.*/
void render_saw_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude) {
    
    int16_t max = 32767 * amplitude;
    float len_cycle = 1 / freq_hz;
    unsigned t = 0;

     for (unsigned i = 0 + channel; i < 2 * num_samples; i += 2) {
        double time = (double)t / SAMPLES_PER_SECOND; 
        double num_cycles = ((double)time) / len_cycle; //Number of cycles that have occured up until this point
	    double fl = floor(num_cycles); //Number of complete cycles that have occured up until this point
        double fraction_traversed  = num_cycles - fl; //Fraction of the current cycle that has been traversed
        double pressure = -1.000000 + 2.000000 * fraction_traversed; 
        
	    int32_t scaled_p = pressure * max;
		
        //Clamp the pressure at the appropriate maximum values if they are exceeded
        if (buf[i] + (int16_t)scaled_p > 32767) {
            buf[i] = 32767;
        } else if (buf[i] + (int16_t)scaled_p < -32768) {
            buf[i] = -32768;
        }else {
            buf[i] += (int16_t)scaled_p;
		}

	    t++;
    }  
}

/**Method to render saw waves for both channels.*/
void render_saw_wave_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude) {
    //Call both render_saw_wave with both channels:  0 for left, 1 for right  
    render_saw_wave(buf, num_samples, 0, freq_hz, amplitude);
    render_saw_wave(buf, num_samples, 1, freq_hz, amplitude);
}

/**Method to render the waves specified by voice in the appropriate channel.*/
void render_voice(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude, unsigned voice) {
    //Sine wave
    if (voice == 0) { 
        render_sine_wave(buf, num_samples, channel, freq_hz, amplitude);
    } 
    //Square wave
    else if (voice == 1) { 
        render_square_wave(buf, num_samples, channel, freq_hz, amplitude);
    }
    //Saw wave
    else if (voice == 2) {
        render_saw_wave(buf, num_samples, channel, freq_hz, amplitude);
    }
}

/**Method to render the waves specified by voice in both channels.*/
void render_voice_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude, unsigned voice) {
    //Sine wave
    if (voice == 0) {
        render_sine_wave_stereo(buf, num_samples, freq_hz, amplitude);
    }
    
    //Square wave
    else if (voice == 1) {
        render_square_wave_stereo(buf, num_samples, freq_hz, amplitude);
    }
    
    //Saw wave
    else if (voice == 2) {
        render_saw_wave_stereo(buf, num_samples, freq_hz, amplitude);
    }
}
