/**Shiyu Li (sli140) 
Munachiso Igboko (migboko1)
*/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "io.h"
#include "wave.h"

int main(int argc, char *argv[]) {
    if (argc != 6) {
        fatal_error("Error: missing necessary parameters.\n");
        return 1;
    }
    
    //Read in command-line arguments
    
    unsigned voice;
    if (sscanf(argv[1], "%u", &voice) != 1) {
        fatal_error("Invalid wave argument.\n");
    }

    float freq_hz;
    if (sscanf(argv[2], "%f", &freq_hz) != 1) {
        fatal_error("Invalid frequency.\n");
    }
	float amplitude;
    if (sscanf(argv[3], "%f", &amplitude) != 1) {
        fatal_error("Invalid amplitude.\n");
    }
	
    unsigned num_samples;
    if (sscanf(argv[4], "%u", &num_samples) != 1) {
        fatal_error("Invalid number of samples.\n");
    }
	char *wavefileout = argv[5];

	
	//Allocate array of appropriate size with all values initialized to 0
	int16_t *buf = (int16_t*) calloc((num_samples * 2), sizeof(int16_t));

    //Render appropriate waves in buf
	render_voice_stereo(buf, num_samples, freq_hz, amplitude, voice);
   	
    FILE* out = fopen(wavefileout, "wb");
	
    //Write the wave header to out
    write_wave_header(out, num_samples);

    //Write the pressure values to out
	write_s16_buf(out, buf, (num_samples * 2));	
    
    //Close files & free dynamically allocated memory
	fclose(out);
    free(buf);
	
    return 0;
}
