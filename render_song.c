/**Shiyu Li (sli140) 
Munachiso Igboko (migboko1)
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "io.h"
#include "wave.h"

int main(int argc, char* argv[]) {

    if (argc != 3) {
        fatal_error("Error: missing necessary parameters.\n");
    }

    char* songinput = argv[1]; //Obtain input file from command-line

    char* waveoutput = argv[2]; //Obtain output file from command-line

    FILE* in = fopen(songinput, "r"); 
    FILE* out = fopen(waveoutput, "wb");

    unsigned num_samples;  //Total number of samples in the song
    unsigned sample_per_beat;  //Numbers of samples in one beat
    
    //Obtain num_samples from the first line of the file
    int n = fscanf(in,"%u", &num_samples);
    if (n != 1) {
        fatal_error("Error: failed to take in num_samples.\n");
    }
    
    //Obtain sample_per_beat from the second line of the file
    int b = fscanf(in, "%u", &sample_per_beat);
    if (b != 1) {
        fatal_error("Error: failed to take in sample_per_beat.\n");
    }

    //Write the wave header of the new song file
    write_wave_header(out, num_samples);
    //Allocate appropriate space for the pressure array
    int16_t *buf = calloc((num_samples * 2), sizeof(int16_t));

    //Scan in the first directive
    char directive;
    int c = fscanf(in, " %c", &directive);
    if (c != 1) {
        fatal_error("Error: failed to take in directive.\n");
    }
    unsigned voice = 0; //Default wave is sine
    float amplitude = 0.1; //Default amplitude is 0.1
    unsigned time = 0; 

    //Continuously read in the directives
    while(c != EOF) {
        
        //Note
        if (directive == 'N') { 
            
            float beat;
            int midi_note;
            int bn = fscanf(in, "%f %d", &beat, &midi_note); //Read in midi_note
            
            if (bn != 2) {
                fatal_error("Error: failed to render song.\n");
            }
            
            //Calculate the frequency of the note
            float freq_hz = 440 * pow(2, (float)(midi_note - 69) / 12);
            
            //Calculate the total number of samples for this note given the beat
            unsigned total_beat = sample_per_beat * beat;
            
            //Render the waves in the appropriate index (multiply by 2 as there are 2 channels)
            render_voice_stereo(&buf[2*time], total_beat, freq_hz, amplitude, voice);
            
            //Increment time by the appropriate number of samples (the appropriate beat)
            time += total_beat; 
        
        //Pause
        } else if (directive == 'P') {
            float pause_beat;
            int p = fscanf(in, "%f", &pause_beat); //Read in pause_beat 
            if (p != 1) {
                fatal_error("Error: failed to render song.\n");
            }
            
            //Calculate the total number of samples for this pause given the beat
            unsigned total_pause = pause_beat * sample_per_beat;
            
            //Increment time by the appropriate number of samples (the appropriate beat)
            time += total_pause;
            
        //Voice
        } else if (directive == 'V') { 
            //Update the voice
            int v = fscanf(in, "%u", &voice);
            if (v != 1) {
                fatal_error("Error: failed to render song.\n");
            }
        
        //Chord
        } else if (directive == 'C') { 
            float cbeat;
            int midi_note;
            fscanf(in, "%f", &cbeat);
            fscanf(in, "%d", &midi_note);
            unsigned total_beat = sample_per_beat * cbeat; //Read in cbeat 
            
            //While there are still midi_notes to be read for the current chord
            while (midi_note != 999) {
                
                //Calculate the frequency of the note
                float freq_hz = 440 * pow(2, (float)((midi_note - 69) / 12.0));
                
                //Render the waves in the appropriate index (multiply by 2 as there are 2 channels)
                render_voice_stereo( &buf[ 2 * time], total_beat, freq_hz, amplitude, voice);
                
                //Read in the next midi_note
                fscanf(in, "%d", &midi_note);
            } 
            
            //Increment time by the appropriate number of samples (the appropriate beat)
            time += total_beat;
            
        //Amplitude
        } else if (directive == 'A') { 
        
            float temp_amp = 0.0;
            
            //Obtain the amplitude
            int a = fscanf(in, "%f", &temp_amp);
            if (a != 1) {
                fatal_error("Error: failed to render song.\n");
            }
            
            //Ensure that the amplitude obtained has an appropriate value & update accordingly
            if ((temp_amp < 0) || (temp_amp > 1.0)) {
                fatal_error("Error: amplitude out of bounds.\n");
            } else {
                amplitude = temp_amp;
            }
            
        //Invalid Directive
        } else {
            fatal_error("Invalid directive.\n");
        }
	
	//Read in the next directive
    	c = fscanf(in, " %c", &directive);    
    }
    
    //Write the pressure array to file out
    write_s16_buf(out, &buf[0], num_samples * 2);
    
    //Close files & free dynamically allocated memory
    fclose(in);
    fclose(out);
    free(buf);
}
