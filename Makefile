CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra 

all: render_tone render_echo render_song

render_tone: render_tone.o io.o wave.o -lm
	$(CC) -o render_tone render_tone.o io.o wave.o -lm
render_song: render_song.o io.o wave.o -lm
	$(CC) $(CFLAGS)  -o render_song render_song.o io.o wave.o -lm
render_echo: render_echo.o io.o wave.o -lm
	$(CC) $(CFLAGS) -o render_echo render_echo.o io.o wave.o -lm
io.o: io.c wave.h
	$(CC) $(CFLAGS) -c io.c
wave.o: wave.c wave.h
	$(CC) $(CFLAGS) -c wave.c
render_tone.o: render_tone.c wave.h
	$(CC) $(CFLAGS) -c render_tone.c
render_song.o: render_song.c wave.h
	$(CC) $(CFLAGS) -c render_song.c
render_echo.o: render_echo.c wave.h
	$(CC) $(CFLAGS) -c render_echo.c
clean:
	rm -f *.o render_tone render_song render_echo
