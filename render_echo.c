/**Shiyu Li (sli140) 
Munachiso Igboko (migboko1)
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "io.h"
#include "wave.h"

int main(int argc, char* argv[]) {
    
    if (argc != 5) {
        fatal_error("Invalid string.\n");
    }
    
    //Read in command-line arguments
    char *wavfilein = argv[1];

    char *wavfileout = argv[2];

    int delay;
    if (sscanf(argv[3], "%d", &delay) != 1) {
        fatal_error("Invalid delay.\n");
    }

    float amplitude;
    if (sscanf(argv[4], "%f", &amplitude) != 1) {
        fatal_error("Invalid amplitude.\n");
    }

    //Pointer to store the number of samples in the provided in file
    unsigned *num_samples = malloc(sizeof(unsigned));
    
    //Open designated files
    FILE* out = fopen(wavfileout, "wb");
    FILE* in = fopen(wavfilein, "rb");
    
    //Read wave header of in
    read_wave_header(in, num_samples);
    
    //Write wave header to out
    write_wave_header(out, *num_samples);

    //Allocate array of appropriate size with all values initialized to 0
    int16_t *buf = (int16_t*) calloc((*num_samples * 2), sizeof(int16_t));

    //Read the pressures from the provided input
    read_s16_buf(in, buf, *num_samples * 2);
    
    //Starting from the last sample, modify the pressure value by adding the appropriate scaled pressure
    for (unsigned i = (*num_samples * 2) - 1; i >= (unsigned)(delay *2); i--) {

        buf[i] +=  (int16_t)(amplitude * buf[i - (2*delay)]); //Delay appropriate value

    }
    
    //Write the pressure array to file out 
    write_s16_buf(out, buf, (*num_samples * 2));
    
    //Close files & free dynamically allocated memory
    fclose(in);
    fclose(out);
    free(buf);
    free(num_samples);

}

